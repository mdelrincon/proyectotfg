package com.adoptalife.ws.rest.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.adoptalife.ws.rest.vo.VOUsuario;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Path("/login")
public class ServiceLogin {
	
	private Connection conexion=null;
	
	//Metodo para crear una cuenta
	@POST
	@Path("/crearCuenta")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.TEXT_PLAIN})
	public String crearCuenta (String cuenta){
		VOUsuario user= new Gson().fromJson(cuenta, VOUsuario.class); //convertimos en usuario el json que recibimos
		try {
			//Obtenemos conexion a la base de datos
			if(this.conexion==null) {
				realizaConexion();
			}
			String sql="INSERT into usuarios (`username`,`password`,`correo`,`telefono_contacto`) VALUES ('"+user.getUsuario()+"','"+user.getPassword()+"','"+user.getCorreo()+"','"+user.getTelefono_contacto()+"')";
			PreparedStatement ps = this.conexion.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			ps.executeUpdate();
			ResultSet generatedID = ps.getGeneratedKeys();
			if (generatedID.next()) {
				return "creado";
			}
		}catch(SQLIntegrityConstraintViolationException e) {
			return "usuario_existente";
		}
		catch (SQLException e) {
			System.out.println("Error en sql");
			e.printStackTrace();
		}
		return "no_creado";
	}
	
	//Metodo para loguearte en el servici
	@POST
	@Path("/loginUser")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.TEXT_PLAIN})
	public String loginUsuario (String vo){
		VOUsuario user= new Gson().fromJson(vo, VOUsuario.class); //convertimos en usuario el json que recibimos
		try {
			//Obtenemos conexion a la base de datos
			if(this.conexion==null) {
				realizaConexion();
			}
			//Hacemos la query 
			String sql="SELECT * FROM usuarios where username=\""+user.getUsuario()+"\" and password=\""+user.getPassword()+"\"";
			PreparedStatement ps;
			ps = this.conexion.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return "existe";
			}
		}
	catch (SQLException e) {
		System.out.println("Error en sql");
		e.printStackTrace();
	}
		return "no";
		
	}
	
	
	//Funciones extras
	private void realizaConexion() {
		try {
			this.conexion=ConexionBBDD.conectar(); 
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
}

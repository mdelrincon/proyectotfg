package com.adoptalife.ws.rest.vo;
import com.google.gson.annotations.SerializedName;


public class VOUsuario {
	
	@SerializedName(value="usuario")
	private String usuario;
	
	@SerializedName(value="password")
	private String password;
	
	@SerializedName(value="telefono_contacto")
	private String telefono_contacto;
	
	@SerializedName(value="correo")
	private String correo;
	
	public String getTelefono_contacto() {
		return telefono_contacto;
	}
	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}

package com.adoptalife.ws.rest.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.adoptalife.ws.rest.vo.Animales;
import com.adoptalife.ws.rest.vo.VOUsuario;
import com.google.gson.Gson;

@Path("/animales")
public class ServiceAnimals {
	
	//Atributos
	private Connection conexion=null;
	
	
	
	
	//Servicios
	
	//Devuelve todos los animales en orden de mas nuevo a mas viejo
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public String getAnimales() {
		ArrayList<Animales> listaAnimales= new ArrayList<Animales>();
		try {
			//Obtenemos conexion a la base de datos
			if(this.conexion==null) {
				realizaConexion();
			}
			//Hacemos la query 
			String sql="SELECT * FROM animales order by fecha_actualizacion desc;";
			PreparedStatement ps;
			ps = this.conexion.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				listaAnimales.add(convAnimal(rs));
			}
			return new Gson().toJson(listaAnimales);
		}
		catch (SQLException e) {
			System.out.println("Error en sql");
			e.printStackTrace();
		}
		return "No se pudieron devolver los animales";
		
	}

	
	//Crea un animal
	@POST
	@Produces({MediaType.TEXT_PLAIN})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("/crearAnimal")
	public String crearAnimal(String animalApp) {
		Animales animal= new Gson().fromJson(animalApp, Animales.class); //convertimos en animal el json que recibimos
		try {
			if(this.conexion==null) {
				realizaConexion();
			}
			//Obtenemos la query para hacer el insert a la base de datos
			String sql = getQuery(animal);
			PreparedStatement ps = this.conexion.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			int rowsAffected= ps.executeUpdate();
			ResultSet id = ps.getGeneratedKeys();
			if (id.next()) {
				animal.setId_animal(id.getInt(1));
				return new Gson().toJson(animal);
			}
		}
		catch(Exception e) {
			System.out.println("Error en sql");
			e.printStackTrace();
		}
		return "No se ha podido introducir en la base de datos";
	}
	
	//Datos de un animal
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path("{idanimales}")
	public String getAnimal(@PathParam("idanimales") String id) {
		int idAnimal = Integer.parseInt(id);
		try {
			if(this.conexion==null) {
				realizaConexion();
			}
			String sql="SELECT * FROM animales where `idanimales`="+idAnimal;
			PreparedStatement ps;
			ps = conexion.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new Gson().toJson(convAnimal(rs));
			}
		} catch (SQLException e) {
			System.out.println("Error en sql");
			e.printStackTrace();
		}
		return "No se ha podido ver el animal con id:"+idAnimal;
	}
	
	//borrar un animal
		@DELETE
		@Path("{idanimales}")
		@Produces({MediaType.TEXT_PLAIN})
		public String borrarAnimal(@PathParam("idanimales") String id) {
			int idAnimal = Integer.parseInt(id);
			try {
				if(this.conexion==null) {
					realizaConexion();
				}
				String sql="DELETE FROM animales where `idanimales`="+idAnimal;
				PreparedStatement ps;
				ps = conexion.prepareStatement(sql);
				int rowsAffected=ps.executeUpdate();
				if (rowsAffected == 1) {
					return "Se ha borrado correctamente el animal con id:"+idAnimal;
				}
			} catch (SQLException e) {
				System.out.println("Error en sql");
				e.printStackTrace();
			}
			return "No se ha podido borrar el animal con id:"+idAnimal;
			
		}
	
	
	

	


	//metodos extras
	
	//Realiza la conexion en caso de que no exista
	private void realizaConexion() {
		try {
			this.conexion=ConexionBBDD.conectar(); 
		}catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	//Devuelve un Animal de una fila
	private Animales convAnimal(ResultSet rs) {
		try {
			Animales animal=new Animales(rs.getString("nombre"				),
					rs.getString("animal"               ),
					rs.getString("raza"                 ),
					rs.getString("fecha_nacimiento"     ),
					//rs.getTimestamp("fecha_actualizacion"  ),
					rs.getString("fecha_actualizacion"  ),
					rs.getString("lugar"                ),
					rs.getString("descripcion"          ),
					rs.getString("foto"                 ),
					rs.getString("esterilizado"         ),
					rs.getString("tamanio"              ),
					rs.getString("vacunado"             ),
					rs.getString("telefono_contacto"    ),
					rs.getString("correo_contacto"    ),
					rs.getString("nombre_usuario"      ));
			return animal;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	//Recibe un animal y devuelve una query insert para ese animal
	private String getQuery(Animales animal) {
		if(animal.getFoto()==null) {
			return "INSERT INTO animales (`nombre`, `animal`, `raza`, `fecha_nacimiento`, `fecha_actualizacion`, `lugar`, `descripcion`, `esterilizado`, `tamanio`, `vacunado`, `telefono_contacto`, `correo_contacto`,`nombre_usuario`)"
					+ " VALUES ("+ "'"
					+ animal.getNombre()+"','"
					+ animal.getAnimal()+"','"
					+ animal.getRaza()+"','"
					+ animal.getFecha_nacimiento()+"','"
					+ animal.getFecha_actualizacion()+"','"
					+ animal.getLugar()+"','"
					+ animal.getDescripcion()+"','"
					+ animal.getEsterilizado()+"','"
					+ animal.getTamanio()+"','"
					+ animal.getVacunado()+"','"
					+ animal.getTelefono_contacto()+"','"
					+ animal.getCorreo_contacto()+"','"
					+ animal.getNombre_usuario()+"');";
		}
		else {
			return "INSERT INTO animales (`nombre`, `animal`, `raza`, `fecha_nacimiento`, `fecha_actualizacion`, `lugar`, `descripcion`, `esterilizado`, `tamanio`, `vacunado`, `telefono_contacto`, `correo_contacto`,`foto`,`nombre_usuario`)"
					+ " VALUES ("+ "'"
					+ animal.getNombre()+"','"
					+ animal.getAnimal()+"','"
					+ animal.getRaza()+"','"
					+ animal.getFecha_nacimiento()+"','"
					+ animal.getFecha_actualizacion()+"','"
					+ animal.getLugar()+"','"
					+ animal.getDescripcion()+"','"
					+ animal.getEsterilizado()+"','"
					+ animal.getTamanio()+"','"
					+ animal.getVacunado()+"','"
					+ animal.getTelefono_contacto()+"','"
					+ animal.getCorreo_contacto()+"','"
					+ animal.getNombre_usuario()+"','"
					+ animal.getFoto()+"');";
		}
	
	}
	
}

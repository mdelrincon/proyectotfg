package com.adoptalife.ws.rest.vo;

import java.sql.Timestamp;

import com.google.gson.annotations.SerializedName;

public class Animales {
	
	//Atributos
	@SerializedName("nombre")
	private String nombre				;
	
	@SerializedName("animal")
	private String animal               ;
	
	@SerializedName("raza")
	private String raza                 ;
	
	@SerializedName("fecha_nacimiento")
	private String fecha_nacimiento     ;
	
	@SerializedName("fecha_actualizacion")
	private String fecha_actualizacion  ;
	
	@SerializedName("lugar")
	private String lugar                ;
	
	@SerializedName("descripcion")
	private String descripcion          ;
	
	@SerializedName("foto")
	private String foto                 ;
	
	@SerializedName("esterilizado")
	private String esterilizado         ;
	
	@SerializedName("tamanio")
	private String tamanio              ;
	
	@SerializedName("vacunado")
	private String vacunado             ;
	
	@SerializedName("telefono_contacto")
	private String telefono_contacto    ;
	
	@SerializedName("correo_contacto")
	private String correo_contacto      ;
	
	@SerializedName("nombre_usuario")
	private String nombre_usuario;
	
	@SerializedName("id_animal")
	private int id_animal;

	

	//Constructor
	public Animales(String nombre, String animal, String raza, String fecha_nacimiento, String fecha_actualizacion,
			String lugar, String descripcion, String foto, String esterilizado, String tamanio, String vacunado,
			String telefono_contacto, String correo_contacto, String nombre_usuario) {
		super();
		this.nombre = nombre;
		this.animal = animal;
		this.raza = raza;
		this.fecha_nacimiento = fecha_nacimiento;
		this.fecha_actualizacion = fecha_actualizacion;
		this.lugar = lugar;
		this.descripcion = descripcion;
		this.foto = foto;
		this.esterilizado = esterilizado;
		this.tamanio = tamanio;
		this.vacunado = vacunado;
		this.telefono_contacto = telefono_contacto;
		this.correo_contacto = correo_contacto;
		this.nombre_usuario=nombre_usuario;
	}
	
	//Getters and setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAnimal() {
		return animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}


	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getFecha_actualizacion() {
		return fecha_actualizacion;
	}

	public void setFecha_actualizacion(String fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getEsterilizado() {
		return esterilizado;
	}

	public void setEsterilizado(String esterilizado) {
		this.esterilizado = esterilizado;
	}

	public String getTamanio() {
		return tamanio;
	}

	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}

	public String getVacunado() {
		return vacunado;
	}

	public void setVacunado(String vacunado) {
		this.vacunado = vacunado;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public String getCorreo_contacto() {
		return correo_contacto;
	}

	public void setCorreo_contacto(String correo_contacto) {
		this.correo_contacto = correo_contacto;
	}
	
	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}
	
	public int getId_animal() {
		return id_animal;
	}

	public void setId_animal(int id_animal) {
		this.id_animal = id_animal;
	}
	
}

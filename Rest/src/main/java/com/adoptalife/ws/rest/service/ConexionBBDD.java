package com.adoptalife.ws.rest.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConexionBBDD {
	
	public static final String CONTROLADOR="com.mysql.jdbc.Driver";
	public static final String URL="jdbc:mysql://localhost:3306/adopta_life";
	public static final String username="mdelrincon";
	public static final String password="Alcaniz2021";
	
	//metodo para obtener la conexion a la base de datos adopta_life
	public static Connection conectar() {
		Connection conn=null;
		
		try {
			Class.forName(CONTROLADOR);
			conn=DriverManager.getConnection(URL,username, password);
			
			System.out.println("Conexion OK");
			
		} catch (ClassNotFoundException e) {
			System.out.println("Error al cargar el controlador.");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Error al obtener la conexion.");
			e.printStackTrace();
		}
		
		return conn;
		
	}
	
	/*public static void main(String []args) {
		try {
			Connection conexion=conectar();
			String sql="SELECT * FROM usuarios;";
			PreparedStatement ps;
			ps = conexion.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				System.out.println(rs.getString("username ")+rs.getString("password"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
}
